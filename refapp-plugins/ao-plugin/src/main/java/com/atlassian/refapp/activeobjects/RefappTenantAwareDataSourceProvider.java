package com.atlassian.refapp.activeobjects;

import com.atlassian.activeobjects.spi.AbstractTenantAwareDataSourceProvider;
import com.atlassian.activeobjects.spi.DatabaseType;
import com.atlassian.refapp.api.ConnectionProvider;
import com.atlassian.tenancy.api.Tenant;
import com.google.common.collect.ImmutableMap;

import javax.annotation.Nonnull;
import javax.sql.DataSource;
import java.util.Map;

import static java.util.Objects.requireNonNull;

public class RefappTenantAwareDataSourceProvider extends AbstractTenantAwareDataSourceProvider {
    private final ConnectionProvider connectionProvider;

    private static final Map<String, DatabaseType> TYPE_MAPPING = ImmutableMap.<String, DatabaseType>builder()
            .put("org.h2.Driver", DatabaseType.H2)
            .put("org.postgresql.Driver", DatabaseType.POSTGRESQL)
            .put("com.mysql.jdbc.Driver", DatabaseType.MYSQL)
            .put("com.microsoft.sqlserver.jdbc.SQLServerDriver", DatabaseType.MS_SQL)
            .put("oracle.jdbc.OracleDriver", DatabaseType.ORACLE)
            .build();

    public RefappTenantAwareDataSourceProvider(@Nonnull final ConnectionProvider connectionProvider) {
        this.connectionProvider = requireNonNull(connectionProvider);
    }

    @Nonnull
    @Override
    public DataSource getDataSource(@Nonnull final Tenant tenant) {
        return connectionProvider.dataSource();
    }

    @Nonnull
    @Override
    public DatabaseType getDatabaseType(@Nonnull final Tenant tenant) {
        final DatabaseType databaseType = TYPE_MAPPING.get(connectionProvider.driverClassName());
        if (databaseType == null) {
            throw new RuntimeException("unknown driver class name '" + connectionProvider.driverClassName() + "'");
        }
        return databaseType;
    }

    @Override
    public String getSchema(@Nonnull Tenant tenant) {
        return connectionProvider.schema().orElse(null);
    }
}
