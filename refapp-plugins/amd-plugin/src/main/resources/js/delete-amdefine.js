// This deletes define.amd, in order to make anonymous modules work.
// Ideally anonymous modules should never be present in the browser; however this done by products and old
// AUI versions.
// We will remove this once AUI gets its shit together (AUI 5.7) and does not include anonymous AMD modules.
((typeof define === "function") && (define.amd = undefined));