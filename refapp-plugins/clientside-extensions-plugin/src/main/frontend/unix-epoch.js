function getLabel() {
    const time = new Date();
    const hours = time.getHours();
    const minutes = time.getMinutes();
    const seconds = time.getSeconds();
    return `The current time is ${hours}:${minutes < 10 ? `0${minutes}` : minutes}:${seconds}`;
}

/**
 * @clientside-extension
 * @extension-point index.links
 */
export default api => {
    setInterval(() => {
        api.updateAttributes({
            label: getLabel(),
        });
    }, 1000);

    return {
        type: 'link',
        label: getLabel(),
        url: 'https://www.timeanddate.com/worldclock/',
    };
};
