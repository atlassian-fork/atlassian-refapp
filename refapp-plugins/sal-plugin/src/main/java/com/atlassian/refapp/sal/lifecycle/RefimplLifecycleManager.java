package com.atlassian.refapp.sal.lifecycle;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.sal.api.lifecycle.LifecycleManager;
import com.atlassian.sal.core.lifecycle.DefaultLifecycleManager;
import org.osgi.framework.BundleContext;

import javax.inject.Named;

@Named("salLifecycleManager")
@ExportAsService(LifecycleManager.class)
public class RefimplLifecycleManager extends DefaultLifecycleManager {
    public RefimplLifecycleManager(
            final PluginEventManager pluginEventManager,
            final PluginAccessor pluginAccessor,
            final BundleContext bundleContext) {
        super(pluginEventManager, pluginAccessor, bundleContext);
    }

    public boolean isApplicationSetUp() {
        return true;
    }
}
