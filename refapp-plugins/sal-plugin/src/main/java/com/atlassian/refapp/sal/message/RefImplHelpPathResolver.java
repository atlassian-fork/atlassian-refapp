package com.atlassian.refapp.sal.message;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.sal.api.message.HelpPath;
import com.atlassian.sal.api.message.HelpPathResolver;

import javax.inject.Named;

@ExportAsService
@Named("helpPathResolver")
public class RefImplHelpPathResolver implements HelpPathResolver {
    /**
     * RefApp doesn't have any specific help available for any plugins.
     */
    public HelpPath getHelpPath(String key) {
        return null;
    }
}
