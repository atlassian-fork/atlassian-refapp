/**
 * Takes xml from an atom feed and returns an object containing the relevant bits. 
 * This file is copied from streams-aggregator-plugin, see atlassian/atlassian-streams on bitbucket,
 * git show 589355f8c0ce0f14361d308b213bd71e93ec471c:aggregator-plugin/src/main/resources/js/atom/jatom.js
 * @method jAtom
 * @param {String} xml String representation of the feed xml
 * @return {Object} Object representation of the feed
 */
function jAtom(xml) {

    /**
     * Cross-browser method for getting an XML object from XHR response
     * @method getXml
     * @param {String|Object} data The xml representation as a string if IE, or as an object if another browser
     * @return {Object} XML object
     */
    function getXml(data) {
        if (typeof data == "string") {
            try {
                // We ask IE to return a string to get around it not liking the 'application/atom+xml' MIME type
                var xml = new ActiveXObject("Microsoft.XMLDOM");
                xml.async = false;
                xml.loadXML(data);
                return xml;
            } catch (e) {
                AJS.log('Failed to create an xml object from string: ' + e);
                return data;
            }
        } else {
            // Other browsers will already return an xml object
            return data;
        }
    }

    /**
     * Creates an object representing a feed entry item
     * @method feedItem
     * @return {Object}
     */
    function feedItem() {
        var authors = new Array();
        return {
            authors: authors,
            title: '',
            links: {},
            timedOutSources: [],
            summary: '',
            content: '',
            updated: '',
            timezoneOffset: '',
            id: '',
            category: '',
            type: '',
            generator: ''
        };
    }

    /**
     * Creates an object representing a feed entry author
     * @method feedPerson
     * @return {Object}
     */
    function feedPerson() {
        return {
            name: '',
            email: '',
            uri: '',
            username: '',
            photos: ''
        };
    }

    /**
     * Creates an object representing an applink authorisation request.  Note, the properties of
     * this object correspond to what is expected by {@code ActivityStreamsApplinks.createAuthRequestBanner()}.
     * @method authMessage
     * @return {Object}
     */
    function authMessage() {
        return {
            id: '',
            appName: '',
            appUri: '',
            authUri: ''
        };
    }
    
    /**
     * Parses an "author" node from feed xml and returns an object representing that person
     * @method parsePerson
     * @return {Object}
     */
    function parsePerson(xml) {
        var person = feedPerson(),
            avatars,
            avatarsLength;
        xml = AJS.$(xml);
        person.name = xml.find('> name').text();
        person.email = xml.find('> email').text();
        person.uri = xml.find('> uri').text();
        person.username = getElementWithNamespace(xml, '>', "usr", "username").text();
        avatars = xml.find("> link[rel='photo']");
        avatarsLength = avatars.length;
        if (avatarsLength) {
            person.photos = [];
            for (var i = 0; i < avatarsLength; i++) {
                var avatar = AJS.$(avatars[i]);
                person.photos.push({
                    uri: avatar.attr('href'),
                    width: avatar.attr('media:width'),
                    height: avatar.attr('media:height')
                });
            }
        }
        return person;
    }

    /**
     * Parses the <link> child elements from element.
     * @method parseLinks
     */
    function parseLinks(element) {
      var links = {};
      element.find('> link').each(function() {
          var link = AJS.$(this),
              rel = link.attr("rel"),
              href = link.attr("href"),
              relLinks = links[rel] || [];
          relLinks.push(href);
          links[rel] = relLinks;
      });
      return links;
    }

    /**
     * Parses <atlassian:authorisation-message> elements, if any.
     * @method parseAuthMessages
     */
    function parseAuthMessages(element) {
        var authMessages = [];
        getElementWithNamespace(element, '>', "atlassian", "authorisation-message").each(function() {
            var reqElement = AJS.$(this),
                item = authMessage();
            item.id = AJS.$.trim(getElementWithNamespace(reqElement, '>', "atlassian", "application-id").text());
            item.appName = getElementWithNamespace(reqElement, '>', "atlassian", "application-name").text();
            item.appUri = AJS.$.trim(getElementWithNamespace(reqElement, '>', "atlassian", "application-uri").text());
            item.authUri = AJS.$.trim(getElementWithNamespace(reqElement, '>', "atlassian", "authorisation-uri").text());
            authMessages.push(item);
        });
        return authMessages;
    }

    function getElementWithNamespace(parent, prefix, namespace, nodeName) {
        var element = parent.find(prefix + namespace + "\\:" + nodeName); // Most browsers work with the namespace
        if( element.length == 0 ) {
            element = parent.find(prefix + nodeName); // Other browsers work with just the node name
        }
        return element;
    }

    /**
     * Parses feed xml and returns an object representing that feed
     * @method parse
     * @return {Object}
     */
    function parse(xml) {
        var channel = AJS.$('feed:first', xml),
            obj = {
              version: '1.0',
              title: channel.find('> title').text(), 
              self: channel.find('> link[rel="self"]').attr('href'),
              description: channel.find('> subtitle').text(),
              language: channel.attr('xml:lang'),
              updated: channel.find('> updated').text(),
              timezoneOffset: getElementWithNamespace(channel, "> ", "atlassian", "timezone-offset").text(),
              timedOutSources: AJS.$.makeArray(getElementWithNamespace(channel, "> ", "atlassian", "timed-out-source-list").children().map(function () {
                  // Extracts the text of each element of the list
                  return AJS.$(this).text();
              })),
              links: parseLinks(channel),
              authMessages: parseAuthMessages(channel),
              items: new Array()
            },
            fullType;

        AJS.$('entry', xml).each( function() {

            var item = feedItem(),
                entry = AJS.$(this),
                object,
                target;

            item.title = entry.find('> title').text();
            item.links = parseLinks(entry);
            item.iconTitle = entry.find('> link[rel="http://streams.atlassian.com/syndication/icon"]').attr('title');
            item.summary = entry.find('> summary').text();
            item.content = entry.find('> content').text();
            item.updated = entry.find('> updated').text();
            item.timezoneOffset = getElementWithNamespace(entry, '>', "atlassian", "timezone-offset").text();
            item.id = entry.find('> id').text();
            item.category = entry.find('> category').attr("term");
            item.generator = entry.find('> generator').attr('uri');

            object = getElementWithNamespace(entry, '>', "activity", "object");
            fullType = getElementWithNamespace(object, '>', "activity", "object-type").text();
            if (fullType.indexOf('http://streams.atlassian.com/syndication/types/') == 0) {
                item.type = fullType.substring('http://streams.atlassian.com/syndication/types/'.length, fullType.length);
            } else if (fullType.indexOf('http://activitystrea.ms/schema/1.0/') == 0) {
                item.type = fullType.substring('http://activitystrea.ms/schema/1.0/'.length, fullType.length);
            } else {
                //third party object types will probably not start with either of those prefixes
                item.type = fullType;
            }

            if (object.length) {
                item.object = {
                    id: object.find('> id').text(),
                    link: object.find('> link').attr('href'),
                    type: getElementWithNamespace(object, '>', "activity", "object-type").text()
                };
            }

            target = getElementWithNamespace(entry, '>', "activity", "target");
            if (target.length) {
                item.target = {
                    id: target.find('> id').text(),
                    title: target.find('> title').text(),
                    summary: target.find('> summary').text(),
                    link: target.find('> link').attr('href'),
                    type: getElementWithNamespace(target, '>', "activity", "object-type").text()
                };
            }

            item.application = getElementWithNamespace(entry, '>', "atlassian", "application").text();
            item.verb = getElementWithNamespace(entry, '>', "activity", "verb").text();

            entry.find("author").each( function() {
                item.authors.push(parsePerson(this));
            });
            obj.items.push(item);
        });

        return obj;
    }

    return parse(getXml(xml));
}
