package com.atlassian.streams.refapp;

import com.atlassian.templaterenderer.TemplateRenderer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

import static java.util.Collections.singletonMap;

public class RefappStreamsActivityServlet extends HttpServlet {
    private static final String TEMPLATE = "/templates/streams.vm";
    private final TemplateRenderer templateRenderer;

    public RefappStreamsActivityServlet(final TemplateRenderer templateRenderer) {
        this.templateRenderer = templateRenderer;
    }

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {

        render(TEMPLATE, singletonMap("baseURL", getBaseUrl(request)), response);
    }

    private void render(final String template, Map<String, Object> context, final HttpServletResponse response)
            throws IOException {
        response.setContentType("text/html; charset=utf-8");

        templateRenderer.render(template, context, response.getWriter());
    }

    private static String getBaseUrl(HttpServletRequest request) {
        return request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
    }
}
