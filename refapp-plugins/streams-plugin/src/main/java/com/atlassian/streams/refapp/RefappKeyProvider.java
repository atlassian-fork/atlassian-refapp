package com.atlassian.streams.refapp;

import com.atlassian.streams.spi.StreamsKeyProvider;

import static java.util.Collections.singletonList;

public class RefappKeyProvider implements StreamsKeyProvider {

    public static final String REFAPP_KEY = "REFAPP";

    @Override
    public Iterable<StreamsKey> getKeys() {
        return singletonList(new StreamsKey(REFAPP_KEY, "Reference App"));
    }
}

