import React, { FC } from 'react';
import { I18n } from '@atlassian/wrm-react-i18n';

const AdminPage: FC = () => {
    return (
        <>
            <p>{I18n.getText('refapp.admin.description.summary')}</p>
            <p>{I18n.getText('refapp.admin.description.details', 'system.admin/general')}</p>
        </>
    );
};

export default AdminPage;
