package com.atlassian.plugin.refimpl.webresource;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginDisabledEvent;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.plugin.event.events.PluginModuleDisabledEvent;
import com.atlassian.plugin.event.events.PluginModuleEnabledEvent;
import com.atlassian.plugin.refimpl.ContainerManager;
import com.atlassian.plugin.refimpl.ParameterUtils;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.cdn.CDNStrategy;
import com.atlassian.sal.api.component.ComponentLocator;
import com.atlassian.sal.api.features.DarkFeatureManager;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.message.LocaleResolver;
import com.google.common.collect.ImmutableMap;
import io.atlassian.util.concurrent.ResettableLazyReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import java.io.File;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class SimpleWebResourceIntegration implements WebResourceIntegration {
    private static final Logger log = LoggerFactory.getLogger(SimpleWebResourceIntegration.class);

    /**
     * Calculates hash of all the plugins with i18n resources.
     */
    public static class I18nHasher {
        private final ResettableLazyReference<String> hashLazyReference;

        public I18nHasher(PluginAccessor pluginAccessor, PluginEventManager eventManager) {
            hashLazyReference = new ResettableLazyReference<String>() {
                @Override
                protected String create() throws Exception {
                    // Tree Set produces consistent ordering and hashing.
                    Set<String> versions = new TreeSet<>();
                    for (Plugin plugin : pluginAccessor.getEnabledPlugins()) {
                        // It would be better to count only plugins that actually contain i18n resources, but
                        // for simplicity counting all the plugins.
                        versions.add(plugin.getKey() + " v. " + plugin.getPluginInformation().getVersion());
                    }
                    return "" + versions.hashCode();
                }
            };
            eventManager.register(this);
        }

        public String get() {
            return hashLazyReference.get();
        }

        @PluginEventListener
        public void onPluginDisabled(final PluginDisabledEvent event) {
            hashLazyReference.reset();
        }

        @PluginEventListener
        public void onPluginEnabled(final PluginEnabledEvent event) {
            hashLazyReference.reset();
        }

        @PluginEventListener
        public void onPluginModuleEnabled(final PluginModuleEnabledEvent event) {
            hashLazyReference.reset();
        }

        @PluginEventListener
        public void onPluginModuleDisabled(final PluginModuleDisabledEvent event) {
            hashLazyReference.reset();
        }
    }


    private final String systemBuildNumber;
    private final LocaleResolver localeResolver;
    private final I18nResolver i18nResolver;
    private final String refappVersion;
    private final PluginEventManager eventManager;
    private final ThreadLocal<Map<String, Object>> requestCache = ThreadLocal.withInitial(() -> {
        // if it's null, we just create a new one.. tho this means results from one request will affect the next request
        // on this same thread because we don't ever clean it up from a filter or anything - definitely not for use in
        // production!
        return new HashMap<>();
    });
    private final I18nHasher i18nHasher;

    public SimpleWebResourceIntegration(final ServletContext servletContext, final LocaleResolver localeResolver,
                                        final I18nResolver i18nResolver, final PluginEventManager eventManager) {
        this(servletContext, localeResolver, i18nResolver, eventManager, "(unknown)");
    }

    public SimpleWebResourceIntegration(final ServletContext servletContext, final LocaleResolver localeResolver,
                                        final I18nResolver i18nResolver, final PluginEventManager eventManager, final String refappVersion) {
        // we fake the build number by using the startup time which will force anything cached by clients to be
        // reloaded after a restart
        systemBuildNumber = String.valueOf(System.currentTimeMillis());
        this.localeResolver = localeResolver;
        this.i18nResolver = i18nResolver;
        this.eventManager = eventManager;
        this.refappVersion = refappVersion;
        this.i18nHasher = new I18nHasher(ContainerManager.getInstance().getPluginAccessor(), eventManager);
    }

    @Override
    public Locale getLocale() {
        return localeResolver.getLocale();
    }

    @Override
    public String getI18nText(final Locale locale, final String s) {
        return i18nResolver.getText(locale, s);
    }

    @Override
    public String getI18nRawText(final Locale locale, final String s) {
        return i18nResolver.getRawText(locale, s);
    }

    @Override
    public CDNStrategy getCDNStrategy() {
        return null;  // Null implies that a CDN is not used
    }

    public String getBaseUrl() {
        return getBaseUrl(UrlMode.AUTO);
    }

    public String getBaseUrl(UrlMode urlMode) {
        // The context path is read from a sys property and might be "/" context path-less instances.
        // for web resources this causes relative paths to be created as //blah so it needs to be replaced by ""
        String baseUrl = ParameterUtils.getBaseUrl(urlMode);
        return "/".equals(baseUrl) ? "" : baseUrl;
    }

    public PluginAccessor getPluginAccessor() {
        return ContainerManager.getInstance().getPluginAccessor();
    }

    public Map<String, Object> getRequestCache() {
        return requestCache.get();
    }

    public String getSystemBuildNumber() {
        return systemBuildNumber;
    }

    public String getSystemCounter() {
        return "1";
    }

    public String getSuperBatchVersion() {
        return "1";
    }

    public String getStaticResourceLocale() {
        log.warn("`getStaticResourceLocale` is deprecated and not used anymore!");
        return getLocale().toString();
    }

    @Override
    public String getI18nStateHash() {
        return i18nHasher.get();
    }

    public File getTemporaryDirectory() {
        final String tempDir = System.getProperty("java.io.tmpdir");
        return new File(tempDir);
    }

    public Map<String, String> getResourceSubstitutionVariables() {
        String pdl = System.getProperty("pdl.dir", "");

        if (pdl.length() > 0 && !pdl.endsWith("/")) { // "pdl.dir" is meant to look like a dir
            pdl += "/";
        }
        return ImmutableMap.of("pdl.dir", pdl);
    }

    @Override
    public boolean useAsyncAttributeForScripts() {
        return ComponentLocator.getComponent(DarkFeatureManager.class)
                .isEnabledForAllUsers("ENABLE_ASYNC_SCRIPTS")
                .orElse(Boolean.FALSE);
    }

    @Override
    public PluginEventManager getPluginEventManager() {
        return this.eventManager;
    }

    @Override
    public Set<String> allowedCondition1Keys() {
        return null;
    }

    @Override
    public Set<String> allowedTransform1Keys() {
        return null;
    }

    @Override
    public boolean forbidCondition1AndTransformer1() {
        return false;
    }

    @Override
    public String getHostApplicationVersion() {
        return refappVersion;
    }

    @Override
    public Iterable<Locale> getSupportedLocales() {
        return localeResolver.getSupportedLocales();
    }
}
