package com.atlassian.plugin.refimpl.tenant;

import com.atlassian.plugins.landlord.spi.LandlordRequestException;
import com.atlassian.plugins.landlord.spi.LandlordRequests;
import com.google.common.util.concurrent.SettableFuture;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * This class provides RefApp specific methods for handling new tenant requests from the Landlord plugin ( which
 * currently arrive via REST calls )
 */
public class RefappLandlordRequests implements LandlordRequests {

    private SettableFuture<Void> tenantTrigger;
    private RefappTenantRegistry tenantRegistry;

    public RefappLandlordRequests(final SettableFuture<Void> tenantTrigger, final RefappTenantRegistry tenantRegistry) {
        this.tenantTrigger = tenantTrigger;
        this.tenantRegistry = tenantRegistry;
    }

    @Override
    public void acceptTenant(final String tenantId) throws LandlordRequestException {
        acceptTenant(tenantId, Collections.<String, String>emptyMap());
    }

    @Override
    public void acceptTenant(final String tenantId, final Map<String, String> tenantProperties) throws LandlordRequestException {
        tenantTrigger.set(null);
        tenantRegistry.setTenant(new RefappTenant(tenantId));
    }

    @Override
    public void removeTenant(final String tenantId) throws LandlordRequestException {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public List<String> getTenants() {
        final List<String> tenantIDs = new ArrayList<String>();
        for (final RefappTenant tenant : tenantRegistry.getRefappTenants()) {
            tenantIDs.add(tenant.getTenantID());
        }
        return tenantIDs;
    }
}
