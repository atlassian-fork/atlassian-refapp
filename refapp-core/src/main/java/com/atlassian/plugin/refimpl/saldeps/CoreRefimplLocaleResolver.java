package com.atlassian.plugin.refimpl.saldeps;

import com.atlassian.sal.api.message.LocaleResolver;
import com.atlassian.sal.api.user.UserKey;

import javax.servlet.http.HttpServletRequest;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

/**
 * This is a copy of com.atlassian.refapp.sal.message.RefimplLocaleResolver. It's been copied because it's required
 * by {@link com.atlassian.plugin.refimpl.ContainerManager} *before* the plugin system is available.
 */
public class CoreRefimplLocaleResolver implements LocaleResolver {
    @Override
    public Locale getLocale(HttpServletRequest request) {
        String country = request.getParameter("locale.country");
        String lang = request.getParameter("locale.lang");
        String variant = request.getParameter("locale.variant");

        if (lang != null && country != null && variant != null) {
            return new Locale(lang, country, variant);
        } else if (lang != null && country != null) {
            return new Locale(lang, country);
        } else if (lang != null) {
            return new Locale(lang);
        } else if (request.getLocale() != null) {
            return request.getLocale();
        }

        return getLocale();
    }

    @Override
    public Locale getLocale() {
        return Locale.getDefault();
    }

    @Override
    public Locale getLocale(UserKey userKey) {
        return Locale.getDefault();
    }

    @Override
    public Set<Locale> getSupportedLocales() {
        final Set<Locale> ret = new HashSet<Locale>();
        ret.add(new Locale("en", "AU"));
        ret.add(Locale.US);
        ret.add(Locale.ENGLISH);
        ret.add(Locale.FRENCH);
        ret.add(Locale.GERMAN);
        return ret;
    }
}
