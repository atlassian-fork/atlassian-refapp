package com.atlassian.plugin.refimpl;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Class used to store and access the currently running http request in the refapp.
 */

public class CurrentHttpRequest {
    private static final ThreadLocal<HttpServletRequest> request = new ThreadLocal<HttpServletRequest>();
    private static final ThreadLocal<HttpServletResponse> response = new ThreadLocal<HttpServletResponse>();

    public static ServletContext getContext() {
        return getRequest().getSession().getServletContext();
    }

    /**
     * Gets the current http request that is stored in this class (each time a new request comes in this is changed).
     */

    public static HttpServletRequest getRequest() {
        return request.get();
    }

    /**
     * @param httpRequest the request
     */
    public static void setRequest(HttpServletRequest httpRequest) {
        request.set(httpRequest);
    }

    /**
     * @param httpResponse the response
     */
    static void setResponse(HttpServletResponse httpResponse) {
        response.set(httpResponse);
    }

    public static HttpServletResponse getResponse() {
        return response.get();
    }
}
