# Atlassian Reference Application (REFAPP)

## Description

The Atlassian Reference Application, or reference implementation, is known as the 'RefApp'. It is a
model implementation of the Atlassian Plugin Framework 2. It takes the form of a simple web
application, which implements the plugin framework and does not do much more than that.

## Ownership

This project is owned by the Server Java Platform team (go/abracadabra).

## Atlassian Developer?

All processes, guides, plans and notes can be found here: 

 * [All Documentation](https://ecosystem.atlassian.net/wiki/display/REFAPPDEV/Home)

Quick links:

 * [Committing - The Platform Rules of Engagement (go/proe)](http://go.atlassian.com/proe)
 * [EcoBAC](https://ecosystem-bamboo.internal.atlassian.com/browse/REFAPP)


## External User?

 * [Issues](https://ecosystem.atlassian.net/browse/REFAPP)
 * [Docs](https://developer.atlassian.com/display/DOCS/About+the+Atlassian+RefApp)

### Quick start

 * cd <project root>
 * If you haven't yet done so, [set up your Atlassian maven environment](https://bitbucket.org/atlassian/maven-settings/)
 * mvn clean install
 * cd refapp-webapp
 * mvn amps:run
 * vi target/refapp.log
 * Browse to [http://localhost:5990/refapp/](http://localhost:5990/refapp/)
 
## External Database

Refapp will spawn an in-process H2 database on an available port. The JDBC URL, username and password for connecting to this database will be output to the logs.

Alternatively, the user may specify an actual external database via java system properties. See com.atlassian.refapp.api.ConnectionProvider for more information.

An example AMPS configuration follows, which will set these properties for an external PostgreSQL database:

    <plugin>
        <groupId>com.atlassian.maven.plugins</groupId>
        <artifactId>amps-maven-plugin</artifactId>
        ...
        <configuration>
            ...
            <systemProperties>
                <refapp.jdbc.external>true</refapp.jdbc.external>
                <refapp.jdbc.driver.class.name>org.postgresql.Driver</refapp.jdbc.driver.class.name>
                <refapp.jdbc.app.url>jdbc:postgresql://localhost:5432/refappdb</refapp.jdbc.app.url>
                <refapp.jdbc.app.schema>refappschema</refapp.jdbc.app.schema>
                <refapp.jdbc.app.user>refappuser</refapp.jdbc.app.user>
                <refapp.jdbc.app.pass>refapppass</refapp.jdbc.app.pass>
            </systemProperties>
            <libArtifacts>
                <libArtifact>
                    <groupId>org.postgresql</groupId>
                    <artifactId>postgresql</artifactId>
                    <version>9.4.1207</version>
                </libArtifact>
            </libArtifacts>
        </configuration>
    </plugin>

Note that when using an external database, it is up to the user to ensure that it is available. This can be easily done via the [sql-maven-plugin](http://www.mojohaus.org/sql-maven-plugin/index.html).

The following example will provision an external PostgreSQL database, as per the above configuration:

    <plugin>
        <groupId>org.codehaus.mojo</groupId>
        <artifactId>sql-maven-plugin</artifactId>
        <version>1.5</version>

        <configuration>
            <skip>false</skip>
            <driver>org.postgresql.Driver</driver>
            <url>jdbc:postgresql://localhost:5432/postgres</url>
            <username>postgres</username>
            <password>postgres</password>
            <autocommit>true</autocommit>
        </configuration>

        <executions>
            <!-- first invocation - drop the existing database if it's there -->
            <execution>
                <id>drop-db-before-test</id>
                <phase>process-test-resources</phase>
                <goals>
                    <goal>execute</goal>
                </goals>
                <configuration>
                    <sqlCommand>
                      drop database if exists refappdb;
                      drop user if exists refappuser;
                    </sqlCommand>
                </configuration>
            </execution>
            <!-- second invocation - create the database -->
            <execution>
                <id>create-db-before-test</id>
                <phase>process-test-resources</phase>
                <goals>
                    <goal>execute</goal>
                </goals>
                <configuration>
                    <sqlCommand>
                        create user refappuser with password 'refapppass';
                        create database refappdb with owner refappuser;
                    </sqlCommand>
                </configuration>
            </execution>
            <!-- third invocation - create the schema as the application user -->
            <execution>
                <id>create-schema-before-test</id>
                <phase>process-test-resources</phase>
                <goals>
                    <goal>execute</goal>
                </goals>
                <configuration>
                    <url>jdbc:postgresql://localhost:5432/refappdb</url>
                    <username>refappuser</username>
                    <password>refapppass</password>
                    <sqlCommand>
                        create schema refappschema;
                    </sqlCommand>
                </configuration>
            </execution>
        </executions>

        <dependencies>
            <dependency>
                <groupId>org.postgresql</groupId>
                <artifactId>postgresql</artifactId>
                <version>9.4.1207</version>
                <scope>compile</scope>
            </dependency>
        </dependencies>
    </plugin>
    
See the root pom.xml in this project for examples using different database vendors.

## Branches 

    refapp-3.2.x: tracks the current publicly consumable release of refapp
    master: was forcibly changed to be based on refapp-3.2.x
    refapp-4.0.x-dead: was master for a short period of time and contains the changes from Platform 4
    
Platform 4 contains a version bump for velocity from 1.6 to 1.7. In order to bump that version
changes were made that make it difficult to successfully provide both velocity 1.6 and 1.7 a product
with a plugin eco system that largely requires the old 1.6 version to function. As there are no current
plans to tackle this version incompatibility problem development will focus future work from before
the changes were made. (at this time the 3.2 branch)

refapp-4.0.x-dead is considered a dead branch that will no receive forward merges from refapp-3.2.x or master.
